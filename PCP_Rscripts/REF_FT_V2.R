## Encoding: UTF-8
## 
## Auswertungsskript Versuch REF
## 
## Automatisierte Auswertung von Weisslicht-Reflexionsdaten mittels 
## Fouriertransformation für Schichtdickenbestimmung
## 
## Autor: Jan P. Tschopp (tschoppj@student.ethz.ch)
## Mit Dank an Erich Meister
## 
## v1.0 (18.10.2013)
## v2.0 (21.11.2014) von Livio Ciorciaro (livioc@ethz.ch):
## - Zusätzliche Kommentare und verbesserte Darstellung des Codes.
## - Fehler korrigiert: Schichtdicke in um (Mikrometer), nicht nm.
## - Abschätzung des Messfehlers durch Peakbreite (Halbwertsbreite).
## - Darstellung der Schichtdicke und des Messfehlers im Plot der Fouriertransformierten.
## - PDF des Plots.

## Erklärung:
## Wegen der Interferenz der Reflexionen an der oberen und unteren Grenze der Schicht ist das 
## Spektrum mit cos(w*c/lambda) = cos(w*f) moduliert. Durch Fouriertransformation wird die 
## Frequenz w der Modulation bestimmt. Aus der Modulationsfrequenz lässt sich die optische 
## Schichtdicke nd = w*c/2 berechnen (c = Lichtgeschwindigkeit).


# Aufräumen
rm(list=ls())
par(mfrow=c(1,1), mar=c(5,4,2,2)+0.1, las=1)

# Konstanten
c <- 299792458 # in m/s

# Auswählen des Inputbereichs
repeat{
    cat("Einzelmessung (1) oder Zeitreihenmessung (2)?  ")
    choice <- readLines(n=1)
    
    # Einzelmessung
    if(choice==1){
        cat("Dateinamen eingeben: ")
        bName = readLines(n=1)
        name <- sprintf('%s.Master.sample', bName)
        cat("Header vorhanden (y/n)? ")
        head <- readLines(n=1)
        if(head=="y"){
            data <- read.table(file=name, sep="\t", skip=14, nrow=2048)
        } else {
            data <- read.table(file=name, sep="\t")
        }
    break
    }
    
    #Zeitreihenmessung
    if(choice==2){
        cat("Basename (XXX in XXX.time.00000.Master.scope) eingeben: ")
        basename <- readLines(n=1)
        cat("Startzahl (keine führenden 0 eingeben): ")
        start <- readLines(n=1)
        cat("Endzahl (keine führenden 0 eingeben):   ")
        ende <- readLines(n=1)
        for(i in start:ende){
            infilename <- sprintf("%s.time.%05i.Master.scope",basename,i)
            indata <- read.table(file=infilename, sep="\t", skip=14, nrow=2048)
            if(i == start){
                data <- indata
            } else {
                data <- cbind(data,indata[,2])
            }
        }
    break
    }
} # End repeat
cat("Daten eingelesen.\n")


#### Fourier-Transformation #####
# FFT function
myfft <- function(s, dist){	# s:Signal, dist: signal distance of points
	f.NQ <- 1/(dist*2)	    # Nyquist time (s)
	N <- length(s)          # Anzahl Datenpunkte in s(ny)
	S <- fft(s) [1:(N/2+1)]	# FastFT nur erste Haelfte des Spektrums (Symmetrie)
	f <- seq(from=0, to=f.NQ, length=length(S)) # Zeitvektor
	list("Frequenz"=f, "Komplex"=S, "Real"=Re(S), "Imag"=Im(S), "Abs"=abs(S),
	    "Power"=(abs(S))^2)
}


# Das Wellenlängespektrum (S~lambda) wird zuerst auf ein Frequenzspektrum (S~f)
# umgerechnet, da die Maxima dort äquidistant sind, d.h. S(f)~cos(w*f).
lambda <- data[,1]      # in nm; Vektor der Wellenlängen (x-Achse im Spektrum)
f <- c/(lambda*1e-9)    # in Hz; Umgerechnet auf Frequenzen
f <- rev(f)             # Umkehren, damit die Frequenzen aufsteigend geordnet sind.
S <- rev(data[,2])

# Bereich finden, der transformiert werden soll.
lowlim <- min(f)
hilim <- max(f)
cat("Auswahl des zu Fouriertransformierenden Spektrums durch Eingabe des x-Achsen-Intervalls.\n")
plot(f*1e-12, S, type="l", xlim=c(lowlim,hilim) * 1e-12, xlab='f / THz', ylab='Signal')
repeat{
    cat("Tiefsten x-Wert angeben: ")
    lowlim <- scan(n=1)*1e12
    cat("Höchsten x-Wert angeben: ")
    hilim <- scan(n=1)*1e12
    plot(f*1e-12, S, type="l", xlim=c(lowlim,hilim) * 1e-12, xlab='f / THz', ylab='Signal')
    cat("Wiederholen? (y/n): ")
    doagain <- readLines(n=1)
    if(doagain=="n"){
        break
    }
}

cat("Faktor für Zerofilling wählen (empfohlen: 20).\nJe höher der Faktor desto besser die Auflösung des Spektrums.\n")
zerofak <- scan(n=1)
cat("\nSpektren werden jetzt Fouriertransformiert...\n\n")

# Automatische Fouriertransformation auf dem gewählten Intervall
for(i in (2:length(data[1,]))){
	f1 <- f[which(f > lowlim & f < hilim)] # Ausgewählter bereich
	S <- rev(data[,i])
	S1 <- S[which(f > lowlim & f < hilim)]
	
	# Interpolation
	pol <- approx(f1,S1,n=1e4)
	f.pol <- pol$x
	S.pol <- pol$y
	
	# Minus mean damit der grosse Peak bei 0 (das Integral über die gesamte Funktion) 
	# verschwindet; anschliessend zero-filling für bessere Auflösung und Glättung.
	S.pol1 <- c(S.pol-mean(S.pol), rep(S.pol*0,zerofak)) 

	# Fouriertransformation f-Domäne -> t-Domäne
	fftdata <- myfft(S.pol1, diff(f.pol)[1])
	P <- fftdata$Power
	t <- fftdata$Frequenz
	
## Erneute Interpolation für höhere Auflösung und dadurch kleinere Stufen im Plot Schichtdicke 
## vs. Spektrennr.
## ACHTUNG: Lange Rechenzeit. Alternative: grosses Zero-Filling; ebenfalls lange Rechezeit.
# pol = approx(t, P, n=90000)
# P = pol$y
# t = pol$x

	# Speichern
	if(i == 2){
		P.data <- P
	} else {
		P.data <- cbind(P.data,P)
	}
    cat(sprintf("Spektrum %i von %i fouriertransformiert.\n", i-1, length(data[1,])-1))
}
cat("Sämtliche Daten fouriertransformiert.\n\n")

## 2. Maximum der Fouriertransformierten finden #####
# +3. Resultate als Vektor zusammenfassen ###########
if(is.vector(P.data)){
    dim(P.data) <- c(length(P.data),1) # Macht einen Spaltenvektro aus dem Zeilenvektor P
}

limit <- c(0,0)
cat("Tieferes Limit der Schichtdicke in um (z. B. 10): ")
limit[1] <- scan(n=1) # in Mikrometer
cat("Höheres Limit der Schichtdicke um (z. B. 100):  ")
limit[2] <- scan(n=1) # in Mikrometer
limit <- limit*2e-6/c # in s; umgerechnet auf die Zeitachse der Fouriertransformierten.

t.range <- t[which(t > limit[1] & t < limit[2])] # Intervall, auf dem das Maximum gesucht wird.
maxima <- 0

for(i in 1:length(P.data[1,])){ # Stellen der Maxima finden in allen Spektren
	P <- P.data[,i]
	P.range <- P[which(t > limit[1] & t < limit[2])]
	P.range = P.range/max(P.range)                               # Normierung der Intensität
	maxima <- c(maxima,t.range[which(P.range==max(P.range))])    # Doppelte Wegzeit in s
}

maxima <- maxima[-1]        # Erstes Element (= 0) entfernen
laengen <- maxima/2*c *1e6  # Schichtdicken aus Maximumsstellen

# Einzelmessung
if(length(laengen)==1){
    # While-Loop sucht den nächsten Datenpunkt bei halber Peakhöhe (in +x-Richtung) für die 
    # Berechnung des Messfehlers.
    oldDiff = 1000
    maxPos = which(P.range == max(P.range)) # Index des Peakmaximums
    position = maxPos
    while(TRUE){
        if(abs(P.range[position+1] - 0.5) < oldDiff){
        oldDiff = abs(P.range[position] - 0.5)
        position = position + 1
        } else {
            break
        }
    }
    
    halbeBreite = t.range[position] - maxima
    fehlerInt = halbeBreite/2 * c * 1e6 # in Mikrometer
  
    # Output der Resultate: Plot und Konsole
    d.range <- t.range/2*c * 1e6
    plot(d.range, P.range, type="l", xlab=expression(nd/mu*m), ylab='P(nd)')
    
    arrowLen = 0.05*(limit[2]-limit[1])/2 * c * 1e6 # 5% des x-Bereichs
    text((0.8 * (limit[2]-limit[1]) + limit[1]) / 2 * c * 1e6, 0.9, pos=4, 
        labels=bquote(nd == .(sprintf('%.2f', laengen))~mu*m), cex=0.85)
    text(d.range[position]+arrowLen, 0.5, pos=4, 
        labels=bquote(.(sprintf('%.2f', 2*fehlerInt))~mu*m), cex=0.85)
    arrows(d.range[2*maxPos - position] - arrowLen, 0.5, d.range[2*maxPos - position], 
        length=0.07, code=2)
    arrows(d.range[position], 0.5, d.range[position] + arrowLen, length=0.07, code=1)
    
    dev.copy2pdf(file=sprintf('%s-Fourier.pdf',bName), width=8, height=5)
    cat(sprintf('Folgende Datei wurde geschrieben: %s-Fourier.pdf\n', bName))
    
    cat(sprintf("Schichtdicke ist %.2f += %.2f um.\n", laengen, fehlerInt))
    
# Zeitreihenmessung
} else {
    xax <- seq(0, length(P.data[1,]) - 1, 1) * 10 # x-Achse in s (Messungen in 10s-Intervallen)
    plot(xax, laengen, type="o", ylim=c(min(laengen),max(laengen)),
        xlab='t / s', ylab=expression(nd/mu*m))
    
    dev.copy2pdf(file=sprintf('%s-Dicke.pdf', basename), width=8, height=5)

    laengenMatrix = cbind(xax, laengen) # xax in s, laengen in Mikrometer
    write.table(laengenMatrix, file=sprintf('%s-Dicke.dat', basename), 
        append=FALSE, quote=FALSE, row.names=FALSE, col.names=FALSE)
        
    cat(sprintf('Folgende Dateien wurden geschrieben:\n%s-Dicke.pdf\n%s-Dicke.dat\n', basename, 
        basename))

    cat(sprintf('Schichtdicke: nd = %.2f um\n', laengen[1]))    
    cat(sprintf('Maximum: %.2f um\n', max(laengen)))
}
