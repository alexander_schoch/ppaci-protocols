# Fehlerbalken zeichnen in x- und/oder y-Richtung
# zusaetzliche arrows-Argumente koennen uebergeben werden

x.errorbars <- function(x,y,xbar,ebl=0.02,...){
  arrows(x-xbar,y,x+xbar,y,code=3,angle=90,length=ebl,...)
}
y.errorbars <- function(x,y,ybar,ebl=0.02,...){
  arrows(x,y-ybar,x,y+ybar,code=3,angle=90,length=ebl,...)
}
xy.errorbars <- function(x,y,xbar,ybar,ebl=0.02,...){
  arrows(x-xbar,y,x+xbar,y,code=3,angle=90,length=ebl,...)
  arrows(x,y-ybar,x,y+ybar,code=3,angle=90,length=ebl,...)
}

