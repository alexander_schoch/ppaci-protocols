# Physical-chemistry Practicum
# Experiment PHM
# Calculation of titration curves, species concentration & Sillen diagram
# Version R
# Dec 2015, ERME

rm(list=ls())    # remove all objects from workspace

#------------------------------------------------------------------------
#------------------------------------------------------------------------
# Starting conditions: changes should be made here

pK1 <- 9.5	# pKa of equilibrium HB/B
pK2 <- 2.5      # pKa of equilibrium H2B/HB
pK3 <- -8       # pKa of equilibrium H3B/H2B
cB0 <- 0         # starting concentration of added B [mol/L]
cHB0 <- 0.0     # starting concentration of added HB [mol/L]
cH2B0 <- 0.05     # starting concentration of added H2B [mol/L]
cH3B0 <- 0.0     # starting concentration of added H3B [mol/L]
cH0 <- 0.0      # starting concentration of added stronc acid [mol/L]
cOH0 <- 0.0      # starting concentration of added strong base [mol/L]
V0 <- 100       # starting volume of solution to be titrated [mL]

cHtit0 <- 0.0	# concentration of strong acid titrating solution [mol/L]
cOHtit0 <- 1.0   # concentration of strong base titrating solution [mol/L]

pHstart <- 0.0	# lower limit of pH range to be calculated
pHstop <- 14    # upper limit of pH range to be calculated
points <- 701   # number of pH data points to be calculated in pH range

#------------------------------------------------------------------------
#------------------------------------------------------------------------
# for details in this section see practicum book

K1 <- 10^(-pK1)
K2 <- 10^(-pK2)
K3 <- 10^(-pK3)
beta1 <- K1
beta2 <- K1 * K2
beta3 <- K1 * K2 * K3
Kw <- 1.0e-14

cBtotal0 <- cB0 + cHB0 + cH2B0 + cH3B0
cHtotal0 <- cH0 - cOH0 + cHB0 + 2 * cH2B0 + 3 * cH3B0

pH <- seq(pHstart, pHstop, length.out=points)
cH <- 10^(-pH)
cOH <- Kw / cH
P <- cH - Kw / cH
Q <- cH / beta1 + 2 * cH^2 / beta2 + 3 * cH^3 / beta3
R <- 1 + cH / beta1 + (cH^2) / beta2 + (cH^3) / beta3
Vtit <- V0 * (cBtotal0 * Q / R + P - cHtotal0) / (cHtit0 - cOHtit0 - P)
titgrada <- (cHtit0 / cBtotal0) * (Vtit / V0)
titgradb <- (cOHtit0 / cBtotal0) * (Vtit / V0)

cB <- (cBtotal0 / R) * (V0 / (V0 + Vtit))
cHB <- cB * cH / beta1
cH2B <- cB * cH^2 / beta2
cH3B <- cB * cH^3 / beta3

cB.Sillen <- cBtotal0 / R
cHB.Sillen <- cB.Sillen * cH / beta1
cH2B.Sillen <- cB.Sillen * cH^2 / beta2
cH3B.Sillen <- cB.Sillen * cH^3 / beta3

#-----------------------------------------------------------------------------

# Plot titration curve with experimental and calculated data

# store default graphics settings before changes are made
oldpar <- par(no.readonly=TRUE)

par(plt=c(0.2,0.85,0.4,0.9),
  cex=0.9,las=1,mgp=c(2.2,0.9,0),
  xaxs="i", yaxs="i"
)

# Graphics with calculated titration curve
plot(NULL, NULL, xlim=c(0,20), ylim=c(0,14), xlab="V / mL", ylab="pH")
lines(Vtit, pH, lwd=2)

xy <- par("usr")

par(xpd=NA) # clipping region
par(cex=1.0)
bla <- "\n\nParameters and starting conditions:"
text(xy[1], xy[3]-(xy[4]-xy[3])*0.23, bla, cex=0.7, adj=c(0,1))

bla <- ""
bla <- sprintf("%s\n  pK1 = %.3f", bla, pK1)
bla <- sprintf("%s\n  pK2 = %.3f", bla, pK2)
bla <- sprintf("%s\n  pK3 = %.3f", bla, pK3)
bla <- sprintf("%s\n  Kw = %.3e", bla, Kw)
text(xy[1]+(xy[2]-xy[1])*0.15, xy[3]-(xy[4]-xy[3])*0.322, bla, cex=0.7, adj=c(0,1))

bla <- ""
bla <- sprintf("%s\n  [B]0 = %.3f M", bla, cB0)
bla <- sprintf("%s\n  [HB]0 = %.3f M", bla, cHB0)
bla <- sprintf("%s\n  [H2B]0 = %.3f M", bla, cH2B0)
bla <- sprintf("%s\n  [H3B]0 = %.3f M", bla, cH3B0)
text(xy[1]+(xy[2]-xy[1])*0.4, xy[3]-(xy[4]-xy[3])*0.322, bla, cex=0.7, adj=c(0,1))

bla <- ""
bla <- sprintf("%s\n  [H]0 = %.3f M", bla, cH0)
bla <- sprintf("%s\n  [OH]0 = %.3f M", bla, cOH0)
bla <- sprintf("%s\n  V0 = %.3f mL", bla, V0)
bla <- sprintf("%s\n  [Htit]0 = %.3f M", bla, cHtit0)
bla <- sprintf("%s\n  [OHtit]0 = %.3f M", bla, cOHtit0)
text(xy[1]+(xy[2]-xy[1])*0.65, xy[3]-(xy[4]-xy[3])*0.322, bla, cex=0.7, adj=c(0,1))

bla <- sprintf("%s", date())
text(xy[1], xy[3]-(xy[4]-xy[3])*0.6, bla, cex=0.7, adj=c(0,1))

par(oldpar)  # recall default graphics settings

# PDF copy of graphics titration curve:
filename <- "PHM-Titration.pdf"
dev.copy2pdf(file=filename, height=7, width=7)
cat("Titration curve graphics stored as PDF with filename \"", filename, "\"\n", sep="")

#-----------------------------------------------------------------------------

# Write numerical titration data to file
filename <- "PHM-Titration.dat"
write.table(data.frame(Vtit, pH, cH, cOH, cB, cHB, cH2B, cH3B),
  file=filename, row.names=F, col.names=T, quote=F)
cat("Titration curve data written to text file \"", filename, "\"\n", sep="")

#-----------------------------------------------------------------------------

# Write Sillen diagram numerical data to file
filename <- "PHM-Sillen.dat"
write.table(data.frame(pH, cH, cOH, cB.Sillen, cHB.Sillen, cH2B.Sillen, cH3B.Sillen),
  file=filename, row.names=F, col.names=T, quote=F)
cat("Sillen diagram data written to text file \"", filename, "\"\n\n", sep="")

#-----------------------------------------------------------------------------

# To interpolate pH at a given volume use:
Vq <- 0                                                 # Volume given
pHq <- approx(Vtit, pH, Vq)$y                           # pH calculated
cat(sprintf("At V = %7.3f mL  pH = %6.3f\n", Vq, pHq), sep="")   # print out

#-----------------------------------------------------------------------------


