﻿# PPAC-Protocols

All Reports for the Physical/Analytical Chemistry Lab course by Asbjørn Rasmussen and Alexander Schoch.

## Experiments

- 01: [BLP] Blitzlichtphotolyse
- 02: [QCS] Quantenmechanische Studien
- 03: [ELK] Elektronik
- 04: [OSP] Oberflächenspannung
- 05: [SDG] Siedediagramm

## Reproduction

All included files can be found in the latex-include directory. For information about how to use them, visit [my templates repository](https://gitlab.com/alexander_schoch/templates)

# License

All protocols are made by Asbjørn Rasmussen and Alexander Schoch and are licensed under [CC-BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (see the LICENSE file). 

