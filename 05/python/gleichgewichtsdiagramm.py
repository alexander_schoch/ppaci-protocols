# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
import scipy
from math import *
from scipy.optimize import curve_fit,least_squares

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')
#plt.rc('text.latex', preamble=r'\usepackage{chemfig}, \usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/data.csv'
OUTPUT = '../R/data_meister.csv'

# constants

M_EtOH   = 46.069 # [g/mol]
M_tol    = 92.141 # [g/mol]

## constants by Ashling and Sarina

L12 = 0.19787
L21 = 0.34125

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    Vtol  = np.array([float(line.split(' ')[0]) for line in lines]) # [mL]
    VEtOH = np.array([float(line.split(' ')[1]) for line in lines]) # [mL]
    ndl   = np.array([float(line.split(' ')[2]) for line in lines]) # []
    ndg   = np.array([float(line.split(' ')[3]) for line in lines]) # []
    T     = np.array([float(line.split(' ')[4]) for line in lines]) # [C]

# read quadratic fit from calibration

with open ('cal.txt') as cal_file:
    parms_str = cal_file.read().split(' ')
    parms = np.array([float(i) for i in parms_str])


a = parms[0]
b = parms[1]
cl = parms[2] - ndl
cg = parms[2] - ndg
dl = (b**2) - (4*a*cl)
dg = (b**2) - (4*a*cg)


# find two solutions
xl = (-b+np.sqrt(dl))/(2*a) 
xg = (-b+np.sqrt(dg))/(2*a)

xlet = 1 - xl
xget = 1 - xg

#xl = np.roots([parms[0], parms[1], parms[2] - ndl])
#xg = np.roots([parms[0], parms[1], parms[2] - ndg])

#xl = np.array([np.roots([parms[0], parms[1], parms[2] - i])[1] for i in ndl])
#xg = np.array([np.roots([parms[0], parms[1], parms[2] - i])[1] for i in ndg])

plt.scatter(xl, xg, s=2, color='black', label=r'measured data')


# print result to intermediate file
file = open(OUTPUT, 'w')
for i in range(0, len(xl) - 1):
    file.write(str(T[i]) + ' ')
    file.write(str(xlet[i]) + ' ')
    file.write(str(xget[i]) + '\n')

file.close()



#plt.xlim(0,1)

# Axis labels
plt.xlabel(r'$x_\text{tol}^\text{(l)}$', fontsize=16)
plt.ylabel(r'$x_\text{tol}^\text{(g)}$', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/gleichgewichtsdiagramm.pdf')
