# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.stats import t
from math import *
from scipy.optimize import curve_fit,least_squares

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/cal.csv'
DATA = '../data/data_ci.csv'
CI = '../data/ci.csv'

# constants

T        = 20.3   # [C]
M_EtOH   = 46.069 # [g/mol]
M_tol    = 92.141 # [g/mol]
rho_tol  = 0.8623 # [g/mL]
rho_EtOH = 0.7893 # [g/mL]


# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen     = decomment(dat)
    lines   = list(gen)
    phiEtOH = np.array([float(line.split(' ')[0]) for line in lines]) # [mL]
    nd      = np.array([float(line.split(' ')[1]) for line in lines]) # []

# Read file and save the data in the 'time' and 'voltage' lists
with open (DATA) as dat:
    gen     = decomment(dat)
    lines   = list(gen)
    nl  = np.array([float(line.split(' ')[0]) for line in lines]) # [C]
    ng  = np.array([float(line.split(' ')[1]) for line in lines]) # [C]
    xl = np.array([float(line.split(' ')[2]) for line in lines]) # []
    xg = np.array([float(line.split(' ')[3]) for line in lines]) # []

phitol = 1 - phiEtOH
V_EtOH = phiEtOH
V_tol = phitol

m_EtOH = rho_EtOH * V_EtOH
m_tol  = rho_tol  * V_tol
n_EtOH = m_EtOH   / M_EtOH
n_tol  = m_tol    / M_tol 

x_tol  = n_tol / (n_tol + n_EtOH)

# quadratic fit

def sqfit (x, a, b, c):
    return a * x**2 + b * x + c

parms, covariance = curve_fit(sqfit, x_tol, nd)

xarr = np.arange(-1, 2, 0.0001)

stder = np.sqrt(np.diag(covariance))

# confidence intervals

def ci (sig, n):
    qt = t.interval(0.95, 10, loc=0, scale=1)[1] 
    return qt * sig / np.sqrt(n) 

nmax = sqfit(xarr, parms[0] + ci (stder[0], 11), parms[1] + ci (stder[1], 11), parms[2] + ci (stder[2], 11))
nmin = sqfit(xarr, parms[0] - ci (stder[0], 11), parms[1] - ci (stder[1], 11), parms[2] - ci (stder[2], 11))

# confidence intervals of our data

def midnight(n, a, b, c):
    d = b**2 - 4 * a * (c - n)
    return (-b + np.sqrt(d))/(2 * a)

cilp = midnight(nl, parms[0] + ci(stder[0], 11), parms[1] + ci(stder[1], 11), parms[2] + ci (stder[2], 11))
cilm = midnight(nl, parms[0] - ci(stder[0], 11), parms[1] - ci(stder[1], 11), parms[2] - ci (stder[2], 11))
cigp = midnight(ng, parms[0] + ci(stder[0], 11), parms[1] + ci(stder[1], 11), parms[2] + ci (stder[2], 11))
cigm = midnight(ng, parms[0] - ci(stder[0], 11), parms[1] - ci(stder[1], 11), parms[2] - ci (stder[2], 11))

# print result to intermediate file for confidence intervals
file = open(CI, 'w')
for i in range(0, len(cilp)):
    file.write(str(cilp[i]) + ' ')
    if (isnan(cilm[i])):
        file.write(str(2 * nl[i] - cilp[i]) + ' ')
    else:
        file.write(str(cilm[i]) + ' ')
    file.write(str(cigp[i]) + ' ')
    file.write(str(cigm[i]) + '\n')

file.close()


# azeotopic point

az = 1 - 0.785
naz = sqfit(az, *parms)

old = 1
for v in np.arange(0,1,0.001):
    val = sqfit(v, parms[0] + ci(stder[0], 11), parms[1] + ci(stder[1], 11), parms[2] + ci (stder[2], 11))
    if (abs(val - naz) < old):
        old = abs(val - naz)
        save = v

old = 1
for v in np.arange(0,1,0.001):
    val = sqfit(v, parms[0] - ci(stder[0], 11), parms[1] - ci(stder[1], 11), parms[2] - ci (stder[2], 11))
    if (abs(val - naz) < old):
        old = abs(val - naz)
        save2 = v


m = (save + save2)/2
ci = save2 - m
print(str(1-m) + ' +- ' + str(ci))
