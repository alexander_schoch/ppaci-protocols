# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.stats import t
from math import *
from scipy.optimize import curve_fit,least_squares

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')
#plt.rc('text.latex', preamble=r'\usepackage{chemfig}, \usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/cal.csv'

# constants

T        = 20.3   # [C]
M_EtOH   = 46.069 # [g/mol]
M_tol    = 92.141 # [g/mol]
rho_tol  = 0.8623 # [g/mL]
rho_EtOH = 0.7893 # [g/mL]


# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen     = decomment(dat)
    lines   = list(gen)
    phiEtOH = np.array([float(line.split(' ')[0]) for line in lines]) # [mL]
    nd      = np.array([float(line.split(' ')[1]) for line in lines]) # []

phitol = 1 - phiEtOH
V_EtOH = phiEtOH
V_tol = phitol

m_EtOH = rho_EtOH * V_EtOH
m_tol  = rho_tol  * V_tol
n_EtOH = m_EtOH   / M_EtOH
n_tol  = m_tol    / M_tol 

x_tol  = n_tol / (n_tol + n_EtOH)

plt.scatter(x_tol, nd, s=2, color='black', label='measured data')

# quadratic fit

def sqfit (x, a, b, c):
    return a * x**2 + b * x + c

parms, covariance = curve_fit(sqfit, x_tol, nd)

xarr = np.arange(x_tol[-1], x_tol[0], 0.0001)
plt.plot(xarr, sqfit(xarr, *parms), linewidth=0.5, color='black', label='quadratic regression')

stder = np.sqrt(np.diag(covariance))

# confidence intervals

def ci (sig, n):
    qt = t.interval(0.95, 10, loc=0, scale=1)[1] 
    return qt * sig / np.sqrt(n) 

nmax = sqfit(xarr, parms[0] + ci (stder[0], 11), parms[1] + ci (stder[1], 11), parms[2] + ci (stder[2], 11))
nmin = sqfit(xarr, parms[0] - ci (stder[0], 11), parms[1] - ci (stder[1], 11), parms[2] - ci (stder[2], 11))

plt.fill_between(xarr, nmax, nmin, alpha=0.5, color='#aaaaaa', label=r'\SI{95}{\percent} confidence interval')

# print result to console
print('-----------------------------')
print('Fit function parameters for ax^2 + bx + c: ')
print('  a: ' + str(parms[0]) + ' +- ' + str(stder[0]))
print('  b: ' + str(parms[1]) + ' +- ' + str(stder[1]))
print('  c: ' + str(parms[2]) + ' +- ' + str(stder[2]))
print('-----------------------------')

# print result to intermediate file
file = open('cal.txt', 'w')
file.write(str(parms[0]) + ' ')
file.write(str(parms[1]) + ' ')
file.write(str(parms[2]))
file.close()


# Axis labels
plt.xlabel(r'$x_\text{tol}$', fontsize=16)
plt.ylabel(r'$n_\text{D}$', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/cal.pdf')
