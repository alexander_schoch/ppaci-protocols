# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# Axis labels
plt.xlabel(r'Time / ms', fontsize=16)
plt.ylabel(r'Voltage / V', fontsize=16)

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open ('../data/Optim_flash_duration.dat') as dat:
#with open ('../data/Verification_900V_toluene_1.dat') as dat:
    gen = decomment(dat)
    lines = list(gen)
    time = [float(line.split(' ')[0]) for line in lines]
    voltage = [float(line.split(' ')[1]) for line in lines]

# Get the time of the flash (=t0)
i = 0
while (time[i] < 0):
    t0 = i
    i += 1 

# Calculate y values at height 50% and 90%
ymin = stat.mean(voltage[0:t0])
ymax = max(voltage)
yavg = (ymax + ymin ) / 2
y90 = ymin + ((ymax - ymin) * 0.1 )

# get array index for peak
for i in range (t0,len(time)):
    if (ymax == voltage[i]):
        tpeak = i

# get the corresponding left t values
i = t0
while ( voltage[i] < yavg ):
    ltavg = i
    i = i + 1

i = t0
while ( voltage[i] < y90 ):
    lt90 = i
    i = i + 1

# get the corresponding right t values
i = tpeak
while ( voltage[i] > yavg ):
    rtavg = i
    i = i + 1

i = tpeak
while ( voltage[i] > y90 ):
    rt90 = i
    i = i + 1

# draw the horizontal lines
plt.plot([time[ltavg],time[rtavg]], [yavg,yavg],'-o', c='black', linewidth=0.5, markersize=3)
plt.plot([time[lt90],time[rt90]], [y90,y90],'-o', c='black', linewidth=0.5, markersize=3)

# print the text next to the horizontal lines
plt.text(time[rtavg] + 0.05,voltage[rtavg] - 0.05,r'FWHM')
plt.text(time[rt90] + 0.05,voltage[rt90] - 0.05,r'FWTM')

# print the line length to the console
print('FWHM: ' + str(time[rtavg] - time[ltavg]) + ' ms')
print('FWTM: ' + str(time[rt90] - time[lt90]) + ' ms')


# Draw a dotted grid
#plt.grid(color='gray', linestyle=':', linewidth=0.2)

# Restrict the y range 
#plt.ylim(-0.5,2)

# Generate a plot with a continous line
plt.plot(time, voltage, c='black', linewidth=0.5)

# Save the plot as a pdf file in the plots directory
plt.tight_layout()
plt.savefig('../plots/9_1.pdf')
