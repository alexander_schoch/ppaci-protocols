import numpy as np
import statistics as stat


#FILENAME = 'Dye_0_1_V3_900V_45.dat'

mixtures = ['0_1','1_1','1_2','1_3','1_4'] 
measurements = ['V1','V2','V3']

# List containing all filenames with data
#with open ('contents.txt') as f:
#    lines = f.readlines()
#    filenames = [line.replace('\n','') for line in lines]

# List to store all mean values of k for the five mixtures
k_mean = [0,0,0,0,0]

# List to store all standard deviations of k for the five mixtures
k_stdev = [0,0,0,0,0]

for mi in [0,1,2,3,4]:
    mix = mixtures[mi]
    
    k = [0,0,0]
    sk = [0,0,0]
    for m in [0,1,2]:
        meas = measurements[m]
        FILENAME = 'Dye_' + mix + '_' + meas + '_900V.dat'

        # ------------------
        # Read meister's fit.
        # ------------------
        # c = k
        # a/b/c are the constants, sa/sb/sc the standard deviations.

        with open ('../data/output_meister/' + FILENAME + '.txt') as f:
            file = f.readlines()
            a = float(file[5].split(' ')[4]) 
            b = float(file[6].split(' ')[4])
            c = float(file[7].split(' ')[4])
            sa = float(file[5].split(' ')[5].replace('(','').replace(')',''))  
            sb = float(file[6].split(' ')[5].replace('(','').replace(')',''))  
            sc = float(file[7].split(' ')[5].replace('(','').replace(')',''))  

        k[m] = c
        sk[m] = sc

    k_mean[mi] = stat.mean(k)
    k_stdev[mi] = stat.stdev(k)

    print(sk)

