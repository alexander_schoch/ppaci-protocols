# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
from decimal import *

#FILENAME = 'Dye_1_1_V1_900V.dat'
FILENAME = 'Dye_0_1_V1_900V.dat'
LINEAR = False

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
s = ""
with open ('../data/' + FILENAME) as dat:
    gen = decomment(dat)
    lines = list(gen)
    time = np.array([float(line.split(' ')[0]) for line in lines])
    voltage = [float(line.split(' ')[1]) for line in lines]
# ------------------
# Read meister's fit
# ------------------

with open ('../data/output_meister/' + FILENAME + '.txt') as f:
    file = f.readlines()
    #a = Decimal(file[5].split(' ')[4]) 
    #b = Decimal(file[6].split(' ')[4]) 
    #c = Decimal(float(file[7].split(' ')[4]))
    a = float(file[5].split(' ')[4]) 
    b = float(file[6].split(' ')[4])
    c = float(file[7].split(' ')[4])
 
# Linearization: 
# Fit by Dr. Meister:   A = a + b * e**(-c * t)
#                 -->   ln ( (A - a) / b ) = -c * t
# Transmission: T = V0 / V
# Absorbanz:    A = -lg(T)

# get the list index where the flash starts
i = 0
while (time[i] < 0):
    flash_index = i
    i += 1 

# V0 is the mean of all values in advance of the flash
V0 = np.mean(voltage[0:flash_index])

# Transmission 
T = V0 / voltage
# Absorbance
A = np.log10(T)

# ------------------
# Fit Curve
# ------------------

#plt.xlim(0,time[-1]+1)   
plt.xlim(0,0.4)
#plt.xlim(0,400)
plt.ylim(0,0.3)

if (LINEAR == False):
     # -----------------------------------------------------------
     # Do not linearize --> show the plot according to Dr. Meister
     # -----------------------------------------------------------

    # Axis labels
    plt.xlabel(r'Time / s', fontsize=16)
    plt.ylabel(r'Absorbance $A = -\log\left(\frac{U}{U_0}\right)$', fontsize=16)

    x = np.arange(0.,time[-1],1)
    y = a + b * np.exp(-c * x / 1000)

    # This is the plot, as fitted by Dr. Meister
    plt.plot(x/1000, y, color='red', linewidth=1)
    plt.scatter(time/1000, A, c='black', s=1)
else:
    # ----
    # This does not work at all yet
    # ----
    plt.xlabel(r'Time / s', fontsize=16)
    plt.ylabel(r'$\ln (A)$', fontsize=16)

    #x = np.arange(0.0,time[-1]+1)
    x = np.arange(0.0,7000)
    y = np.log(b) - c * x / 1000

    A_conv = np.log(A[flash_index+3:])

    plt.plot(x/1000, y, color='red', linewidth=1)
    plt.scatter(time[flash_index+3:]/1000, A_conv, c='black', s=1)
    
# Draw a dotted grid
#plt.grid(color='gray', linestyle=':', linewidth=0.2)

# Generate a plot with a continous line

# Save the plot as a pdf file in the plots directory
plt.tight_layout()
plt.savefig('../plots/fitting_' + FILENAME + '.pdf')
plt.savefig('../plots/linearize_lin.pdf')
