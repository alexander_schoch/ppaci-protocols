import numpy as np
import statistics as stat

mixtures = ['1:0','1:1','2:1','3:1','4:1'] 

# constants
sig_m = 0.0001 # [g]

M_tol = 92.141 # [g mol-1]
M_iso = 60.096 # [g mol-1]

# results
m_iso = np.array([0.0,7.7963,3.8815,3.9012,3.9320]) # [g]
m_tol = np.array([42.8696,8.6124,8.6305,13.6387,17.2410]) # [g]
m_dye = np.array([0.0142,0.0047,0.0037,0.0048,0.0054]) # [g]
m_tot = m_iso + m_tol + m_dye

mp = m_dye / m_tot * 100

n_iso = m_iso / M_iso 
n_tol = m_tol / M_tol 
n_tot = n_iso + n_tol

x_iso = n_iso / n_tot
x_tol = n_tol / n_tot

# error propagation

var_n_iso = np.sqrt( sig_m**2 / M_iso )
var_n_tol = np.sqrt( sig_m**2 / M_tol )
var_n_tot = var_n_tol + var_n_iso

var_x_iso = var_n_iso / n_tot + n_iso * var_n_tot / n_tot**2
var_x_tol = var_n_tol / n_tot + n_tol * var_n_tot / n_tot**2

var_m_tot = 3 * sig_m**2
var_mp    = sig_m**2 / m_tot - m_dye * var_m_tot / m_tot**2

# print results
for i in range (0,5):
    print(mixtures[i] + ' --- xtol      = ' + str(x_tol[i]) + ', xiso      = ' + str(x_iso[i]) + ', MP      = ' + str(mp[i]) + '%')
    print(mixtures[i] + ' --- stdevxtol = ' + str(np.sqrt(var_x_tol[i])) + ', stdevvxiso = ' + str(np.sqrt(var_x_iso[i])) + ', stdevMP = ' + str(np.sqrt(var_mp[i]) * 100) + '%')
