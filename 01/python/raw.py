# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# Variables

#FILE = '../data/Optim_Photomoultiplier_ideal.dat'
FILE = '../data/Verification_900V_toluene_1.dat'

# Axis labels
plt.xlabel(r'Time / s', fontsize=16)
plt.ylabel(r'Voltage / V', fontsize=16)

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    time = np.array([float(line.split(' ')[0]) for line in lines])
    voltage = [float(line.split(' ')[1]) for line in lines]

# Draw a dotted grid
# plt.grid(color='gray', linestyle=':', linewidth=0.2)

# Restrict the y range 
plt.ylim(-0.1,2.1)

# Generate a plot with a continous line
plt.plot(time/1000, voltage, c='black', linewidth=0.5)

#Save the plot as a pdf file in the plots directory
plt.tight_layout()
plt.savefig('../plots/raw.pdf')
