
Datei: Dye_1_2_V3_900V.dat
Fitfunktion:  a + b * exp(-c * t)
t-Intervall: 180.000 ... 8830.000 ms
Parameter (Standardabweichung):
  a = -0.00414 (0.00023)
  b = 0.44005 (0.00112)
  c = 8.283e-01 (3.208e-03) s^-1
  tau = 1.207e+00 (4.675e-03) s
