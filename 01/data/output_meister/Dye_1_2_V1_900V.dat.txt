
Datei: Dye_1_2_V1_900V.dat
Fitfunktion:  a + b * exp(-c * t)
t-Intervall: 150.000 ... 7090.000 ms
Parameter (Standardabweichung):
  a = -0.00103 (0.00021)
  b = 0.43029 (0.00086)
  c = 9.080e-01 (2.947e-03) s^-1
  tau = 1.101e+00 (3.574e-03) s
