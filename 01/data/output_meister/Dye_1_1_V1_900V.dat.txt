
Datei: Dye_1_1_V1_900V.dat
Fitfunktion:  a + b * exp(-c * t)
t-Intervall: 140.000 ... 9470.000 ms
Parameter (Standardabweichung):
  a = -0.00663 (0.00019)
  b = 0.37226 (0.00077)
  c = 7.139e-01 (2.454e-03) s^-1
  tau = 1.401e+00 (4.815e-03) s
