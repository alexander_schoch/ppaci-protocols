# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/CR.csv'

# constants

R = 1000.0    # Ohm
C = 1E-8    # Farad

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    index = [line.split(' ')[0] for line in lines]          # index of measurement                      
    UCH1  = [float(line.split(' ')[1]) for line in lines]   # Voltage of Channel 1 (reference)          [ticks]
    UCH2  = [float(line.split(' ')[2]) for line in lines]   # Voltage of Channel 2 ('Ausgangsspannung') [ticks]
    TS    = [float(line.split(' ')[3]) for line in lines]   # Time Shift                                [ticks]
    f     = [float(line.split(' ')[4]) for line in lines]   # Frequency                                 [Hz]
    VCH1  = [float(line.split(' ')[5]) for line in lines]   # Volts/div of Channel 1                    [V]
    VCH2  = [float(line.split(' ')[6]) for line in lines]   # Volts/div of Channel 2                    [V]
    Time  = [float(line.split(' ')[7]) for line in lines]   # Time/div                                  [us]

# get fg from file
with open ('fg.txt') as fg_file:
    for row in fg_file:
        fg = float(row)

# scatter plot
UE = np.array(UCH1) * np.array(VCH1)
UA = np.array(UCH2) * np.array(VCH2)
G = 20 * np.log10( UA / UE )
logx = np.log10( np.array(f) / fg )

plt.xlim(-2,2)
plt.ylim(-35,5)

plt.scatter(logx,G,s=4,color='black')

# curve fitting (round curve)
## fit function. R and C should be optimized
def opt (fopt, copt):
    ropt = R
    return 20 * np.log10(abs( ropt / (ropt + (1 / (1j * fopt * 2 * np.pi * copt)))))

## fit curve
parms, covariance = curve_fit(opt, f, G, p0=[C])

## draw the line
fopt = np.arange(100, 10E6, 10)
xoptlog = [np.log10( i / fg ) for i in fopt]
gopt = [opt(i,*parms) for i in fopt]

plt.plot(xoptlog, gopt, linewidth=0.5, color='black', label='fitted curve')
plt.plot(xoptlog, opt(fopt, C), linewidth=0.5, color='black', linestyle='-.', label='theoretical curve')

# print result

qt    = t.interval(0.95, len(f) - 1, loc=0, scale=1)[1]
varC  = np.diag(covariance)[0]
varfg = varC / (2 * np.pi * R * parms[0]**2 )**2
fg    = 1 / (2 * np.pi * R * parms[0])

print('============================')
print('Parameters:')
print('R  = ' + str(R))
print('C  = ' + str(parms[0]) + ' +- ' + str(np.sqrt(varC) * qt / np.sqrt(len(f))))
print('============================')
print('fg = ' + str(fg) + ' +- ' + str(np.sqrt(varfg) * qt / np.sqrt(len(f))))


# curve fitting (straight lines)
xarr = np.arange(-2,2,0.1)
plt.plot(xarr, 0*xarr, linewidth=0.5, linestyle='--', color='black')

## threshold for f / fg for the linear fit of the data. 
## Points above this threshold will be ignored.
data_threshold = -0.7 
threshold_ind = 0 

## Get array index for this threshold
for i in range(len(f)):
    if (np.log10(f[i] / fg ) < data_threshold):
        threshold_ind = i

## Linear fit for x < 0.0
parms = np.polyfit(logx[0:threshold_ind], G[0:threshold_ind], 1)
m = parms[0]
b = parms[1]

G_fit = xarr * m + b

## Tilted regression line
plt.plot(xarr, G_fit, linewidth=0.5, linestyle='--', color='black')

## Horizontal regression line
yarr = np.arange(-35,10)
plt.plot(0 * yarr, yarr, linewidth=0.5, linestyle='--', color='black')

# Axis labels
plt.xlabel(r'$\log x = \log \left(f / f_\text{g}\right)$', fontsize=16)
plt.ylabel(r'$|G(x)|$ / \si{\deci\bel}', fontsize=16)

#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.5)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/bode.pdf')
