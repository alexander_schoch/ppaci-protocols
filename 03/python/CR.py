# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# variables

FILE = '../data/CR.csv'

# constants

R = 1000.0    # Ohm
C = 1E-8    # Farad

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    index = [line.split(' ')[0] for line in lines]          # index of measurement                      
    UCH1  = [float(line.split(' ')[1]) for line in lines]   # Voltage of Channel 1 (reference)          [ticks]
    UCH2  = [float(line.split(' ')[2]) for line in lines]   # Voltage of Channel 2 ('Ausgangsspannung') [ticks]
    TS    = [float(line.split(' ')[3]) for line in lines]   # Time Shift                                [ticks]
    f     = [float(line.split(' ')[4]) for line in lines]   # Frequency                                 [Hz]
    VCH1  = [float(line.split(' ')[5]) for line in lines]   # Volts/div of Channel 1                    [V]
    VCH2  = [float(line.split(' ')[6]) for line in lines]   # Volts/div of Channel 2                    [V]
    Time  = [float(line.split(' ')[7]) for line in lines]   # Time/div                                  [us]

# scatter plot
UE = np.array(UCH1) * np.array(VCH1)
UA = np.array(UCH2) * np.array(VCH2)
G =  UA / UE

# Axis Limits
plt.xlim(10,1E6)
plt.ylim(-0.05,1.05)

# Logarithmic Scale & Scatter plot
plt.xscale('log')
plt.scatter(f,G,s=4,color='black')

# curve fitting
## fit function. R and C should be optimized
def opt (fopt, copt):
    ropt = R
    return abs( ropt / (ropt + (1 / (1j * fopt * 2 * np.pi * copt ))))

## fit curve: opt is the fit function, f and G are the points the function should fit, p0 are the starting values. 
## the fitted R/C values and the covariances are saved in the parms array and the covariance matrix
parms, covariance = curve_fit(opt, f, G, p0=C)

qt = t.interval(0.95, len(f) - 1, loc=0, scale=1)[1]
varC  = np.diag(covariance)[0]
varfg = varC / (2 * np.pi * R * parms[0]**2 )**2

### Print the fitted values for R and C
print('=========================')
print('Parameters:')
print('R    = ' + str(R))
print('C    = ' + str(parms[0]) + ' +- ' + str(np.sqrt(varC) * qt / np.sqrt(len(f))))
print('=========================')

## draw the line: generate an array of values (xopt) and plug them into the opt function
#xopt = np.arange(10, 1E6, 10)
xopt = np.arange(10, 1E6, 10)
gopt = [opt(i, *parms) for i in xopt]

## plot the values calculated before
plt.plot(xopt, gopt, linewidth=0.5, color='black', label='fitted curve')
plt.plot(xopt, opt(xopt, C), linewidth=0.5, color='black', linestyle='-.', label='theoretical curve')

# find fg

## Horizontal line at G = 1 / sqrt(2)
plt.plot(xopt, 1 / np.sqrt(2) + 0 * xopt, linewidth=0.5, linestyle='--', color='black')

## Find the corresponding f value
y_fg = 1
for i in range(len(gopt)):
    if abs(gopt[i] - 1/np.sqrt(2)) < y_fg:
        y_fg = abs(gopt[i] - 1/np.sqrt(2))
        fg_ind = i

fg = xopt[fg_ind]
fg = 1 / (np.pi * 2 * R * parms[0])

## Print fg
print('fg = ' + str(fg) + ' +- ' + str(np.sqrt(varfg) * qt / np.sqrt(len(f))) + ' Hz')

## Vertical line at f = fg
yval = np.arange(-0.5,1.5,0.01)
plt.plot(fg + yval * 0, yval , linewidth=1, linestyle='--', label=r'fitted $f_\text{g}$')

## Print the value on the graph
#plt.text(fg * 1.1, 0.3, r'$f_\text{g} = \SI{' + str(fg) + r'}{\hertz}$')
plt.text(100, 1 / np.sqrt(2) + 0.03, r'$1 / \sqrt{2}$')

# write fg to file for other scripts
file = open('fg.txt', 'w')
file.write(str(fg))
file.close()


# Axis labels
plt.xlabel(r'$f$ / \si{\hertz}', fontsize=16)
plt.ylabel(r'$|G(f)| = \frac{U_\text{A}}{U_\text{E}}$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/CR.pdf')
