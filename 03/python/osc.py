# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit,least_squares
import scipy
from lmfit import Model

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/osc.csv'

# constants

R = 500.0  # Ohm
C = 1E-7    # Farad
L = 2.32 * 30 * 30 / 1000000 # H
f = 257.1403 # Hz

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    index = np.array([line.split(' ')[0] for line in lines])          # index of measurement                      
    dx    = np.array([float(line.split(' ')[1]) for line in lines])   # delta x                                   [ticks]
    time  = np.array([float(line.split(' ')[2]) for line in lines])   # time / div                                [s]
    y     = np.array([float(line.split(' ')[3]) for line in lines])   # height                                    [ticks]
    volts = np.array([float(line.split(' ')[4]) for line in lines])   # Volts / div                               [V / div]

# scatter plot

#t = np.array(dx) * (np.array(time) / 5)
#V = np.array(y) * (np.array(volts) / 5)

dxdiv = dx / 5
Vdiv = y / 5
t = dxdiv * time # ms
V = Vdiv * volts # V


#plt.xlim(10,1E6)
#plt.ylim(-2.5,2.5)
#
#plt.xscale('log')
plt.scatter(t,V,s=4,color='black')

# fit

def opt(topt, aopt, bopt, copt):
    return aopt * np.exp(-bopt * topt) * np.sin(copt * topt)

def exp(te, a, b):
    return -a * np.exp(-b * te)    

qt = scipy.stats.t.interval(0.95, len(t) - 1, loc=0, scale=1)[1]
parms, covariance = curve_fit(opt, t, V)
varb = np.diag(covariance)[1]
vartau = varb / (parms[1]**2)**2

tau = 1 / parms[1]
print('tau: ' + str(tau) + ' +- ' + str(np.sqrt(vartau) * qt / np.sqrt(len(t))))
print('')
print('a:   ' + str(parms[0])) # [V]
print('b:   ' + str(parms[1])) # [ms-1]
print('c:   ' + str(parms[2])) # [ms-1]
print('confidence intervals: ' + str(np.sqrt(np.diag(covariance)) * qt / np.sqrt(f)))

xarr = np.arange(0.0,t.max(), 0.01)
yarr = [opt(i, *parms) for i in xarr]
ye   = [exp(i, parms[0], parms[1]) for i in xarr]

plt.plot(xarr, yarr, linewidth=0.5, color='black', label=r'$U(t) = ae^{-bt}\sin (ct)$')
plt.plot(xarr, ye, linewidth=0.5, color='black', linestyle='dashed', label=r'$U(t) = \pm ae^{-bt}$')
plt.plot(xarr, -np.array(ye), linewidth=0.5, color='black', linestyle='dashed')

# Axis labels
plt.xlabel(r'$t \text{ / } \si{\milli\second}$', fontsize=16)
plt.ylabel(r'$U \text{ / } \si{\volt}$', fontsize=16)

#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/osc.pdf')
