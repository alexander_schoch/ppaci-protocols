# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit,least_squares
from scipy.stats import t
from lmfit import Model

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

# Variables

FILE = '../data/RLC.csv'

# constants

R = 500.0  # Ohm
C = 1E-7    # Farad
L = 2.32 * 30 * 30 / 1000000 # H


# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    index = [line.split(' ')[0] for line in lines]          # index of measurement                      
    UCH1  = [float(line.split(' ')[1]) for line in lines]   # Voltage of Channel 1 (reference)          [ticks]
    UCH2  = [float(line.split(' ')[2]) for line in lines]   # Voltage of Channel 2 ('Ausgangsspannung') [ticks]
    TS    = [float(line.split(' ')[3]) for line in lines]   # Time Shift                                [ticks]
    f     = [float(line.split(' ')[4]) for line in lines]   # Frequency                                 [Hz]
    VCH1  = [float(line.split(' ')[5]) for line in lines]   # Volts/div of Channel 1                    [V]
    VCH2  = [float(line.split(' ')[6]) for line in lines]   # Volts/div of Channel 2                    [V]
    Time  = [float(line.split(' ')[7]) for line in lines]   # Time/div                                  [us]

# scatter plot
UE = np.array(UCH1) * np.array(VCH1)
UA = np.array(UCH2) * np.array(VCH2)
G =  UA / UE

plt.xlim(10,1E6)
plt.ylim(-0.05,1.05)

plt.xscale('log')
plt.scatter(f,G,s=4,color='black')

# curve fitting
## fit function. R, L and C should be optimized

def opt (fopt, lopt):
    ropt = R
    copt = C
    omega = 2 * np.pi * fopt
    brac = (omega * copt - 1 / (omega * lopt)) ** -2
    num = brac
    denom = ropt * ropt + brac
    return np.sqrt( num / denom )

## fit curve
parms, covariance = curve_fit(opt, f, G, p0=[L])

## draw the line
fopt = np.arange(10, 1E6, 10)
gopt = np.array([opt(i, *parms) for i in fopt])

# *parms = parms[0], parms[1], parms[2]
plt.plot(fopt, opt(fopt, *parms), linewidth=0.5, color='black', label='fitted curve')
plt.plot(fopt, opt(fopt, L), linewidth=0.5, color='black', linestyle='-.', label='theoretical curve')

# Get fres

fres    = 1 / (2 * np.pi * np.sqrt(parms[0] * C))
qt      = t.interval(0.95, len(f) - 1, loc=0, scale=1)[1]
varL    = np.diag(covariance)[0]
varfres = varL / (4 * np.pi * np.sqrt(C) * (parms[0])**(1.5) )**2

#fres = fopt[np.where(gopt == gopt.max())]
print('fres: ' + str(fres))
print('======================')
print('C    = ' + str(C))
print('R    = ' + str(R))
print('L    = ' + str(parms[0]) + ' +- ' + str(np.sqrt(varL) * qt / np.sqrt(len(f))))
print('======================')
print('fres = ' + str(fres) + ' +- ' + str(np.sqrt(varfres) * qt / np.sqrt(len(f))))
yval = np.arange(-0.1, 1.1, 0.1)

plt.plot(fres + yval * 0, yval, color='black', label=r'fitted $f_\text{res}$', linewidth=0.5, linestyle='--')
#plt.text(fres * 1.1, 0.85, r'$f_\text{res} = \SI{' + str(int(fres)) + r'}{\hertz}$')

# Axis labels
plt.xlabel(r'$f$ / \si{\hertz}', fontsize=16)
plt.ylabel(r'$|G(f)| = \frac{U_\text{A}}{U_\text{E}}$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/RLC.pdf')
