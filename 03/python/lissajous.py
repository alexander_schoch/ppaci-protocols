import numpy as np
import matplotlib.pyplot as plt
import sys

# Latex implementation
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{chemfig}, \usepackage{siunitx}')

# constants
x0 = 1.0
y0 = 1.0

fx = 5.0
fy = 3.0

dt = 0.001

#t = np.arange(0.0,float(sys.argv[1])*0.01,dt)
t = np.arange(0.0,500.0,dt)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

plt.xlim(-1.1,1.1)
plt.ylim(-1.1,1.1)

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

plt.text(1.1,0.05,r'$x$')
plt.text(0.05,1.1,r'$y$')

num_ticks = 4
xticks = np.arange(-x0,x0 + x0/num_ticks,x0/num_ticks)
yticks = np.arange(-y0,y0 + y0/num_ticks,y0/num_ticks)
ax.set_xticks(np.delete(xticks, num_ticks))
ax.set_yticks(np.delete(yticks, num_ticks))
# Show ticks in the left and lower axes only
#ax.xaxis.set_ticks_position('bottom')
#ax.yaxis.set_ticks_position('left')


x = x0 * np.cos(2.0 * np.pi * fx * t + np.pi/4)
y = y0 * np.cos(2.0 * np.pi * fy * t)

#for ti in np.arange(0,x.size):
#    print(x[ti],y[ti],ti)

ax.plot(x,y,color='black',linewidth=0.5)

ax.grid(color='gray',linestyle=':')

plt.savefig('../plots/lissajous.pdf')
#plt.savefig('output/' + sys.argv[1] + '.png', dpi=400)
