# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/CR.csv'

# constants

R = 1000.0    # Ohm
C = 1e-8    # Farad
uC = C * 1e6

#qt = t.interval(0.95, 10, loc=0, scale=1)[1] 

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    index = [line.split(' ')[0] for line in lines]          # index of measurement                      
    UCH1  = [float(line.split(' ')[1]) for line in lines]   # Voltage of Channel 1 (reference)          [ticks]
    UCH2  = [float(line.split(' ')[2]) for line in lines]   # Voltage of Channel 2 ('Ausgangsspannung') [ticks]
    TS    = [float(line.split(' ')[3]) for line in lines]   # Time Shift                                [ticks]
    f     = np.array([float(line.split(' ')[4]) for line in lines])   # Frequency                                 [Hz]
    VCH1  = [float(line.split(' ')[5]) for line in lines]   # Volts/div of Channel 1                    [V]
    VCH2  = [float(line.split(' ')[6]) for line in lines]   # Volts/div of Channel 2                    [V]
    Time  = [float(line.split(' ')[7]) for line in lines]   # Time/div                                  [us]

# get fg from file
with open ('fg.txt') as fg_file:
    for row in fg_file:
        fg = float(row)

fg = 10890

# scatter plot
UE = np.array(UCH1) * np.array(VCH1)
UA = np.array(UCH2) * np.array(VCH2)
phiG = -np.arctan([TS[i] * Time[i] / 5 for i in range(len(TS))]) * 180 / np.pi
phiG = -f * Time * TS * 180 / ( np.pi * 10**6 )
#phiG = np.arctan( UA / UE ) * 360 / ( 2 * np.pi )
logx = np.log10( np.array(f) / fg )

plt.xlim(-2,2)
plt.ylim(-95,5)
plt.scatter(np.log10(f/fg),phiG,s=4,color='black')

# curve fitting (round curve)

def opt (fopt, copt, alpha):
    ropt = R
    num = ropt / (ropt + (1 / (1j * fopt * 2 * np.pi * copt))) 
    return alpha * np.arctan(num.imag / num.real) * 180 / np.pi

## fit function. R and C should be optimized
parms, covariance = curve_fit(opt, f, phiG, p0=[C, 1])

## draw the line
fopt = np.arange(100, 10E6, 10)
gopt = np.array([opt(i, *parms) for i in fopt])

plt.plot(np.log10(fopt/fg), opt(fopt, *parms), color='black', linewidth=0.5, label='fitted curve')
plt.plot(np.log10(fopt/fg), opt(fopt, C, -1), color='black', linewidth=0.5, linestyle='-.', label='theoretical curve')

# get fg

plt.plot([-2,2],[-45,-45],linewidth=0.5,linestyle=':',color='red', markersize=0)

i = np.argmin(np.abs(gopt+45))
fgnew = fopt[i]

qt = t.interval(0.95, len(f) - 1, loc=0, scale=1)[1]
varC = np.diag(covariance)[0]
varfg = varC / (2 * np.pi * R * parms[0]**2)**2

print('=========================')
print('Parameters: ')
print('R      = ' + str(R) + ' Ohm')
print('C      = ' + str(parms[0]) + ' +- ' + str(qt * np.sqrt(varC) / np.sqrt(len(f))) + ' F')
print('alpha  = ' + str(parms[1]) + ' +- ' + str(np.sqrt(np.diag(covariance))[1] * qt / np.sqrt(len(phiG))))
print('=========================')
print('fg     = ' + str(fgnew) + ' +- ' + str(np.sqrt(varfg) * qt / np.sqrt(len(f))) + ' Hz')
#print('stdevR = ' + str(R) + 'Ohm')

plt.plot([np.log10(fgnew/fg),np.log10(fgnew/fg)],[-95,5],linewidth=0.5,linestyle=':',color='red',markersize=0, label=r'$\log x$ at $\phi_G = \SI{-45}{\degree}$')

# Axis labels
plt.xlabel(r'$\log x = f / f_\text{g}$', fontsize=16)
plt.ylabel(r'$\phi_G(x)$ / \si{\degree}', fontsize=16)

#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.5)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/phase.pdf')
