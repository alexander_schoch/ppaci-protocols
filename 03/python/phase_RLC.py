# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/RLC.csv'

# constants

R = 500.0    # Ohm
C = 1e-7    # Farad
L = 0.00209 # Henry

# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    index = [line.split(' ')[0] for line in lines]          # index of measurement                      
    UCH1  = [float(line.split(' ')[1]) for line in lines]   # Voltage of Channel 1 (reference)          [ticks]
    UCH2  = [float(line.split(' ')[2]) for line in lines]   # Voltage of Channel 2 ('Ausgangsspannung') [ticks]
    TS    = [float(line.split(' ')[3]) for line in lines]   # Time Shift                                [ticks]
    f     = np.array([float(line.split(' ')[4]) for line in lines])   # Frequency                                 [Hz]
    VCH1  = [float(line.split(' ')[5]) for line in lines]   # Volts/div of Channel 1                    [V]
    VCH2  = [float(line.split(' ')[6]) for line in lines]   # Volts/div of Channel 2                    [V]
    Time  = [float(line.split(' ')[7]) for line in lines]   # Time/div                                  [us]

# get fg from file
#with open ('fg.txt') as fg_file:
#    for row in fg_file:
#        fg = float(row)

fres = 9407

#fg = 9340.0

# scatter plot
UE = np.array(UCH1) * np.array(VCH1)
UA = np.array(UCH2) * np.array(VCH2)
#phiG = -np.arctan([TS[i] * Time[i] / 5 for i in range(len(TS))]) * 180 / np.pi
phiG = -f * Time * TS * 180 / ( np.pi * 10**6 )
#phiG = np.arctan( UA / UE ) * 360 / ( 2 * np.pi )
logx = np.log10( np.array(f) / fres )

plt.xlim(-2,2)
plt.ylim(-95,95)
plt.scatter(np.log10(f/fres),phiG,s=4,color='black')

# curve fitting (round curve)
#schwingkreis_phi <- function(W, L) {
#  R_res <- 500
#  C <- 100*10^-9
#  phi <- atan(R_res*((1/(W*L))-(W*C)))
#  phi*180/pi
#}

def opt (fopt, lopt, alpha):
    ropt = R
    copt = C
    w = 2 * np.pi * fopt
    a = ropt * ((1/(w*lopt))-(w*copt))    
    res = alpha * np.arctan(a) * 180 / np.pi
    return res

## fit function. R and C should be optimized
parms, covariance = curve_fit(opt, f, phiG, p0=[L,1])

## draw the line
fopt = np.arange(100, 10E6, 10)
gopt = np.array([opt(i, *parms) for i in fopt])

plt.plot(np.log10(fopt/fres), opt(fopt, *parms), color='black', linewidth=0.5, label='fitted curve')
plt.plot(np.log10(fopt/fres), opt(fopt, L, -1), color='black', linewidth=0.5, linestyle='-.', label='theoretical curve')

fresnew = 1 / (2 * np.pi * np.sqrt(parms[0] * C))
qt      = t.interval(0.95, len(f) - 1, loc=0, scale=1)[1]
varL    = np.diag(covariance)[0]
varfres = varL / (4 * np.pi * np.sqrt(C) * (parms[0])**(1.5) )**2

#fres = fopt[np.where(gopt == gopt.max())]
print('fres: ' + str(fres))
print('======================')
print('C    = ' + str(C))
print('R    = ' + str(R))
print('L    = ' + str(parms[0]) + ' +- ' + str(np.sqrt(varL) * qt / np.sqrt(len(f))))
print('alpha = ' + str(parms[1]) + ' +- ' + str(np.sqrt(np.diag(covariance)[1]) * qt / np.sqrt(len(f))))
print('======================')
print('fres = ' + str(fresnew) + ' +- ' + str(np.sqrt(varfres) * qt / np.sqrt(len(f))))

# get fg

i = np.argmin(np.abs(gopt))
fgnew = fopt[i]

plt.plot([-2,2],[0,0],linewidth=0.5,linestyle=':',color='red', markersize=0)
plt.plot([np.log10(fgnew/fres),np.log10(fgnew/fres)],[-95,95],linewidth=0.5,linestyle=':',color='red',markersize=0, label=r'$\log x$ at $\phi_G = \SI{0}{\degree}$')

#print('fres = ' + str(fgnew))
#print('R0   = ' + str(R))
#print('C0   = ' + str(C))
#print('L0   = ' + str(L))
#print('L    = ' + str(parms[0]) + ' +- ' + str(np.sqrt(np.diag(covariance)[0])))
#print('C    = ' + str(parms[1]) + ' +- ' + str(np.sqrt(np.diag(covariance)[1])))
#print('L    = ' + str(parms[2]) + ' +- ' + str(np.sqrt(np.diag(covariance)[2])))


# Axis labels
plt.xlabel(r'$\log x = f / f_\text{res}$', fontsize=16)
plt.ylabel(r'$\phi_G(x)$ / \si{\degree}', fontsize=16)

#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.5)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/phase_RLC.pdf')
