# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
import scipy
from math import *
from scipy.optimize import curve_fit,least_squares

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')
#plt.rc('text.latex', preamble=r'\usepackage{chemfig}, \usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/cal.csv'

# constants

num_drop = 15

rho_l = 998.2 # [kg/m^3]
T = 19.7 # °C
rho_g = 1.2041 # [kg / m^3]
rK = 0.00599 / 2 # [m]
g = 9.80652 # [m / s^-2]

qt97_5 = 2.5706

## Error Propagation constants
sig_h = 0.000001 # [m]
sig_r = 1E-5     # [m]
sig_m = 1E-7     # [kg]

## correction factors for phi

a = 0.90585	
b = -0.72779	
c = 0.43221	



# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    m  = np.array([float(line.split(' ')[1]) for line in lines]) / 1000 # [g]
    h  = np.array([float(line.split(' ')[2]) for line in lines]) / 1000 # [m]


# Alpha
dV = m / rho_l # [m^3]
alpha = dV / h # [m^3 / m] = [m^2]
alpha_mean = np.mean(alpha)


sig_alpha = np.std(alpha)
ci95_alpha = qt97_5 * sig_alpha / np.sqrt(len(alpha))
#print('Alpha:  ' + str(alpha_mean * 1000000) + ' +- ' + str(ci95_alpha * 1000000) + ' m^2')
#print('Stdder: ' + str(sig_alpha))

# sigma of water

#V = alpha_mean * h
#rho_l = m / V
#
#def surfTen (mf, phi):
#    return mf * g * (1 - (rho_g / rho_l)) / (2 * np.pi * rK * phi)
#
#
#m_drop = m / num_drop #[kg]
#Vf = m_drop / rho_l # [m^3]
#q = rK / (Vf)**(1/3) # [m^2]
#
#phi = a + b * q + c * q * q
#
#sig = surfTen (m_drop, phi) * 1000# [mN/m]


#print(np.mean(sig), qt97_5 * np.std(sig) / np.sqrt(5))


# Water - isopropanol
FILE = '../data/drop.data.csv'
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    #sol  = np.array([float(line.split(',')[0]) for line in lines])
    #mh2o = np.array([float(line.split(',')[1]) for line in lines])
    #miso = np.array([float(line.split(',')[2]) for line in lines])
    x    = np.array([float(line.split(',')[6]) for line in lines]) # 
    h    = np.array([float(line.split(',')[7]) for line in lines]) / 1000 # [m]
    m    = np.array([float(line.split(',')[8]) for line in lines]) / 1000# [kg]


V = alpha_mean * h
rho_l = m / V

def surfTen (mf, phi):
    return mf * g * (1 - (rho_g / rho_l)) / (2 * np.pi * rK * phi)

m_drop = m / num_drop # [kg]
Vf = m_drop / rho_l   # [m^3]
q = rK / (Vf)**(1/3)  # [m^2]

phi = a + b * q + c * q * q

#print(np.mean(phi))
#print(qt97_5 * np.std(phi) / np.sqrt(12))


sig = surfTen (m_drop, phi)# * 1000# [mN/m]

# scatter plot
plt.scatter(x,sig,s=2,color='black')

# sigred
sig2 = sig[0]
sig1 = sig[-1]
sigred = (sig2 - sig) / (sig2 - sig1)

# fit for sigred

def fitfunc (x1, a, b):
    x2 = 1 - x1
    return (1 + (a * x2)/(1 - b * x2)) * x1

parms, covariance = curve_fit(fitfunc, x, sigred, p0=[9.74, 0.84])

xarr = np.arange(0,1,0.01)
sigred = fitfunc(xarr,*parms)
sigfit = -(sigred * (sig2 - sig1) - sig2)

plt.plot(xarr, sigfit, linewidth=0.5, color='black')

alit = 0.984
blit = 0.970

sigredlit = fitfunc(xarr, alit, blit)
sigfitlit = -(sigredlit * (sig2 - sig1) - sig2)

plt.plot(xarr, sigfitlit, linewidth=0.5, color='red')
#print(*parms)

#plt.xlim(10,1E6)
#plt.ylim(-0.05,1.05)

# Error Propagation

sig_V = np.sqrt( (sig_alpha * h)**2 + (alpha_mean * sig_h)**2)
sig_l = np.sqrt( (sig_m / V )**2 + (sig_V * m * np.log(V))**2 ) * 1000

sum1 = sig_V * g  * (rho_l - rho_g) / (2 * np.pi * rK * phi)
sum2 = sig_r * Vf * g  * (rho_l - rho_g) * log(rK) / (2 * np.pi * phi)
sum3 = sig_l * Vf * g / (2 * np.pi * rK * phi)
sig_sig = np.sqrt(sum1**2 + sum2**2 + sum3**2) #* 1000
plt.errorbar(x, sig, xerr=None, yerr=sig_sig, barsabove=True, fmt='none', color='gray', linewidth=0.1, capsize=3, capthick=0.1)

# Axis labels
plt.xlabel(r'$x_\text{iso}$', fontsize=16)
plt.ylabel(r'$\sigma$ / \si{\newton\per\meter}', fontsize=16)


#print(rho_l[-1], sig_l[-1],sig[-1], sig_sig[-1])

#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.5)

for i in np.arange(0,13):
    print(str(x[i]) + ' & ' + str(sig[i]) + ' & ' + str(sig_sig[i]) + ' & ')

print(x[-1], sig[-1], qt97_5 * sig_sig[-1] / np.sqrt(14))
print(x[1], sig[1], qt97_5 * sig_sig[1] / np.sqrt(14))

#print(*parms)
#print(np.sqrt(np.diag(covariance)))

plt.tight_layout()
#plt.legend()
plt.savefig('../plots/sig.pdf')
