# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
import scipy
from math import *
from scipy.optimize import curve_fit,least_squares

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')
#plt.rc('text.latex', preamble=r'\usepackage{chemfig}, \usepackage{siunitx}')

ax = plt.subplot()

# Variables

FILE = '../data/cal.csv'

# constants

## correction factors for phi

a = 0.90585	
b = -0.72779	
c = 0.43221	

num_drop = 15

rho_l = 998.2 # [kg/m^3]
T = 19.7 # °C
rho_g = 1.2041 # [kg / m^3]
rK = 0.00599 / 2 # [m]
g = 9.80652 # [m / s^-2]
phi = 1

qt97_5 = 2.5706

sig_h = 0.000001 # [m]
sig_r = 1E-5     # [m]
sig_m = 1E-7     # [kg]



# function to decomment a file. it goes through every line and and splits it according to #. Everything before will be passed to var raw.
def decomment(file):
    for row in file:
        raw = row.split('#')[0].strip()
        if raw: yield raw

# Read file and save the data in the 'time' and 'voltage' lists
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    m  = np.array([float(line.split(' ')[1]) for line in lines]) / 1000 # [g]
    dh  = np.array([float(line.split(' ')[2]) for line in lines]) / 1000 # [m]


# Alpha
dV = m / rho_l # [m^3]
alpha = dV / dh # [m^3 / m] = [m^2]
mean_alpha = np.mean(alpha)


sig_alpha = np.std(alpha)
ci95 = qt97_5 * sig_alpha/ np.sqrt(len(alpha))
print('Alpha:  ' + str(mean_alpha) + ' +- ' + str(ci95) + ' m^2')
print('Stdder: ' + str(sig_alpha))
#V = alpha * dh
#rho_l = m / V

# Water - isopropanol
FILE = '../data/drop.data.csv'
with open (FILE) as dat:
    gen = decomment(dat)
    lines = list(gen)
    #sol  = np.array([float(line.split(',')[0]) for line in lines])
    #mh2o = np.array([float(line.split(',')[1]) for line in lines])
    #miso = np.array([float(line.split(',')[2]) for line in lines])
    x    = np.array([float(line.split(',')[6]) for line in lines]) # 
    h    = np.array([float(line.split(',')[7]) for line in lines]) / 1000 # [m]
    m    = np.array([float(line.split(',')[8]) for line in lines]) / 1000# [kg]

V = mean_alpha * h
rho_l = m / V

plt.scatter(x,rho_l,s=2,color='black')

# fit for density

def linreg (x, m, b):
    return m * x + b

parms, covariance = curve_fit(linreg, x, rho_l)

xarr = np.arange(0,1,0.01)
plt.plot(xarr, linreg(xarr,*parms), linewidth=0.5, color='black')

# Error Propagation

sig_V = np.sqrt( (mean_alpha * sig_h)**2 + (h * sig_alpha**2))
sig_rho = np.sqrt( (np.log(V) * m * sig_V)**2 + (sig_m / V)**2)
plt.errorbar(x, rho_l, xerr=None, yerr=sig_rho, barsabove=True, fmt='none', color='gray', linewidth=0.1, capsize=3, capthick=0.1)



#plt.xlim(10,1E6)
#plt.ylim(-0.05,1.05)

# Axis labels
plt.xlabel(r'$x_\text{iso}$', fontsize=16)
#plt.ylabel(r'$\sigma$ / \si{\milli\newton\per\meter}', fontsize=16)
plt.ylabel(r'$\rho_\text{l}$ / \si{\kilo\gram\per\cubic\meter}', fontsize=16)

#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.5)

plt.tight_layout()
#plt.legend()
plt.savefig('../plots/density.pdf')
