OSP Tropfenvolumen-Tensiometer
------------------------------
File                         : ../data/alpha.txt 
n	h	m 
5 Messungen:
15 31.388 1306.1 41.71573
15 31.712 1310.3 41.4223
15 32.149 1322.6 41.2428
15 31.026 1310.9 42.35755
15 31.842 1312.6 41.3256

Resultate (mit Standardabweichungen):
Kalibrationsfaktor [mm^3/mm] :   41.61280 (0.45303) 
Kapillarradius          [mm] :    2.99750 (0.01000) 
Dichte Umgebung    [mg/mm^3] :    0.00120 
Verschiebung            [mm] :    2.10823 (0.02876) 
Masse                   [mg] :   87.50000 (0.40877) 
Volumen               [mm^3] :   87.72844 (1.53151) 
Dichte             [mg/mm^3] :    0.99770 (0.01803) 
Parameter                    :    0.67465 (0.00452) 
Korrekturfaktor              :    0.61158 (0.00065) 
Oberflaechenspannung  [mN/m] :   74.40662 (0.41194) 
Monte Carlo Simulation mit 500000 Samples. 
Thu May 23 17:32:28 2019 
