#!/bin/bash

printf "please enter the number of the experiment\n\n> "
read num
printf "please enter the name of the experiment (no spaces)\n\n> "
read name

mkdir -p $num/src

cp templates/template.tex $num/src/$num_$name.tex
cp templates/template.bib $num/src/literature.bib
cp templates/Makefile $num/
cd $num
make
ln -s src/$num_$name.pdf report.pdf
ln -s src/$num_$name.tex report.tex



