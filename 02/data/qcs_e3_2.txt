\begin{mdframed}
  \centering 
  \begin{longtable}
    \toprule
      Comformere & Molar Fraction $x$ \\ \midrule\midrule
    \endhead
      ZZZsss & 0.986947 \\ \midrule
      ZZZssa & 4.323815e-06 \\ \midrule
      EZZssa & 2.024626e-05 \\ \midrule
      EZZass & 0.01302828 \\ \midrule
      EZZsss & 1.538058e-10 \\ \midrule
      ZZZsas & 7.026273e-19 \\ \midrule
      EZZsas & 7.026311e-19 \\ \midrule
      EZZaas & 4.3743e-25 \\ \midrule
      ZZZass & 1.37772e-07 \\ \bottomrule
  \end{longtable}
\end{mdframed}

\begin{mdframed}
  \centering 
  \begin{longtable}
    \toprule
      Comformere & Molar Fraction $x$ \\ \midrule\midrule
    \endhead
      ZZZsss & 0.9813171 \\ \midrule
      ZZZssa & 1.209768e-05 \\ \midrule
      EZZssa & 4.976864e-05 \\ \midrule
      EZZass & 0.01862047 \\ \midrule
      EZZsss & 1.015958e-09 \\ \midrule
      ZZZsas & 2.322876e-17 \\ \midrule
      EZZsas & 2.322784e-17 \\ \midrule
      EZZaas & 4.792832e-23 \\ \midrule
      ZZZass & 5.146478e-07 \\ \bottomrule
  \end{longtable}
\end{mdframed}

\begin{mdframed}
  \centering 
  \begin{longtable}
    \toprule
      Comformere & Molar Fraction $x$ \\ \midrule\midrule
    \endhead
      ZZZsss & 0.959142
      ZZZssa & 0.0001146942
      EZZssa & 0.0003550818
      EZZass & 0.04037896
      EZZsss & 6.352589e-08
      ZZZsas & 4.987922e-14
      EZZsas & 4.987926e-14
      EZZaas & 1.429789e-18
      ZZZass & 9.203182e-06
  \end{longtable}
\end{mdframed}
